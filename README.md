# OpenWeatherApiexample

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

Download Gitlab from https://git-scm.com/downloads

## Clone Repo

git clone https://neveeran@bitbucket.org/neveeran/angular4-sampleapp.git

## Install Dependencies

Run following commands

npm install yarn -g

yarn

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
